﻿using esend.Globalization;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace esend.Extensions
{
    public static class HtmlExtensions
    {
        private static IResourceService _resources = new ResourceService();

        public static string GetResource(this IHtmlHelper helper, string resourceName, string resourceKey)
        {
            ResourceService resource = new ResourceService();
            return resource.GetResource(resourceName, resourceKey);
        }

        public static void RegisterProvider(IResourceService provider)
        {
            _resources = provider;
        }

        public static bool ShowMainNavigation(string path)
        {
            if (path == "/") // Index
            {
                return false;
            }
            return true;
        }
    }
}

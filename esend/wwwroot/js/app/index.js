﻿"use strict";

/** check if namespace exists */
var esend = esend || {};

/**
 *
 */
esend.index = (function (w, $) {
    var params = {};

    $(window).on('load', function () {
        // initialization of HSScrollNav
        $.HSCore.components.HSScrollNav.init($('#js-scroll-nav'), {
            duration: 700,
            easing: 'easeOutExpo'
        });

        // initialization of cubeportfolio
        $.HSCore.components.HSCubeportfolio.init('.cbp');
    });

    $(document).on('ready', function () {

        params = esend.index.params;
        // initialization of carousel
        $.HSCore.components.HSCarousel.init('.js-carousel');

        $('#carouselCus1').slick('setOption', 'responsive', [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 7
            }
        }, {
            breakpoint: 992,
            settings: {
                slidesToShow: 3
            }
        }, {
            breakpoint: 576,
            settings: {
                slidesToShow: 1
            }
        }], true);

        // initialization of header
        $.HSCore.components.HSHeader.init($('#js-header'));
        $.HSCore.helpers.HSHamburgers.init('.hamburger');

        // initialization of custom select
        $.HSCore.components.HSSelect.init('.js-custom-select');

        // initialization of go to section
        $.HSCore.components.HSGoTo.init('.js-go-to');

        // initialization of chart pies
        var items = $.HSCore.components.HSChartPie.init('.js-pie');

        $('#shipingFrom').magicSuggest({
            allowFreeEntries: false,
            mode: 'remote',
            valueField: 'id',
            displayField: 'description',
            autoSelect: true,
            renderer: function (city) {
                return '<div>' +
                    '<div style="font-family: Arial; font-weight: bold">' + city.structured_formatting.main_text + '</div>' +
                    '<div>' + city.description + '</div>' +
                    '</div>';
            },
            data: params.searchCitiesUrl
        });

        $('#shipingTo').magicSuggest({
            allowFreeEntries: false,
            mode: 'remote',
            valueField: 'id',
            displayField: 'description',
            autoSelect: true,
            renderer: function (city) {
                return '<div>' +
                    '<div style="font-family: Arial; font-weight: bold">' + city.structured_formatting.main_text + '</div>' +
                    '<div>' + city.description + '</div>' +
                    '</div>';
            },
            data: params.searchCitiesUrl
        });
    });

    function getAutocompleteData() {
        var gUrl = "https://maps.googleapis.com/maps/api/place/autocomplete/json",
            a = $("#shipingFrom").val(),
            input = "?input=" + encodeURI("Θεσσαλονίκη"),
            params = "&types=&language=el&types=cities&key=AIzaSyBhlxnUzj2f5Bn4bdtkduZOXmsnTl7pxTQ";

        //return gUrl + input + params;
        return "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%CE%98%CE%B5%CF%83%CF%83%CE%B1%CE%BB%CE%BF%CE%BD%CE%AF%CE%BA%CE%B7&types=&language=el&types=cities&key=AIzaSyBhlxnUzj2f5Bn4bdtkduZOXmsnTl7pxTQ";

    }



    return {
        // view model object
        //params: params,
        // sub-modules
    };
}(window, jQuery));

function initMap() {
    $.HSCore.components.HSGMap.init('.js-g-map');
}









$(function () {




    //$('#ms-tagbox').magicSuggest({
    //    placeholder: 'Enter one or multiple tags'
    //});

    //$('#ms-filter').magicSuggest({
    //    placeholder: 'Select...',
    //    allowFreeEntries: false,
    //    data: [{
    //        id: 1,
    //        name: 'Location',
    //        nb: 34
    //    }, {
    //        id: 2,
    //        name: 'Keyword',
    //        nb: 106
    //    }],
    //    selectionPosition: 'bottom',
    //    selectionStacked: true,
    //    selectionRenderer: function (data) {
    //        return data.name + ' (<b>' + data.nb + '</b>)';
    //    }
    //});

    //$('#ms-scrabble').magicSuggest({
    //    placeholder: 'Type some real or fake fruits',
    //    data: ['Banana', 'Apple', 'Orange', 'Lemon']
    //});

    //$('#ms-emails').magicSuggest({
    //    placeholder: 'Enter recipients...',
    //    data: [{
    //        name: 'Georges Washington',
    //        email: 'georges.washington@whitehouse.gov'
    //    }, {
    //        name: 'Theodore Roosevelt',
    //        email: 'theodore.roosevelt@whitehouse.gov'
    //    }, {
    //        name: 'Benjamin Franklin',
    //        email: 'benjamin.franlin@whitehouse.gov'
    //    }, {
    //        name: 'Abraham Lincoln',
    //        email: 'abraham.lincoln@whitehouse.gov'
    //    }],
    //    valueField: 'email',
    //    renderer: function (data) {
    //        return data.name + ' (<b>' + data.email + '</b>)';
    //    },
    //    resultAsString: true
    //});

    //// note that it would be a lot more proper to use CSS classes here instead of inline style
    //$('#ms-complex-templating').magicSuggest({
    //    data: 'random.json',
    //    renderer: function (data) {
    //        return '<div style="padding: 5px; overflow:hidden;">' +
    //            '<div style="float: left;"><img src="' + data.picture + '" /></div>' +
    //            '<div style="float: left; margin-left: 5px">' +
    //            '<div style="font-weight: bold; color: #333; font-size: 10px; line-height: 11px">' + data.name + '</div>' +
    //            '<div style="color: #999; font-size: 9px">' + data.email + '</div>' +
    //            '</div>' +
    //            '</div><div style="clear:both;"></div>'; // make sure we have closed our dom stuff
    //    }
    //});

});
﻿using esend.Models.Parameter;
using esend.Rest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;

namespace esend.Management.GoogleApisManagement
{
    public static class GoogleApisPlacesManagement 
    {
        public static async Task<List<GoogleApiPlace>> GetPlaces(string query)
        {
            var serializer = new DataContractJsonSerializer(typeof(List<GoogleApiPlace>));
            GoogleApiPlacesResponse response = await RestCallClient.GetPlaces(query);
            List<GoogleApiPlace> places = response.predictions;
            return places;
        }
    }
}

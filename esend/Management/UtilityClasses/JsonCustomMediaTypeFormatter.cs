﻿using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Converters;

namespace esend.Management.UtilityClasses
{
    internal static class JsonCustomMediaTypeFormatter
    {
        //private static JsonMediaTypeFormatter _jsonFormatter = new JsonMediaTypeFormatter();

        //// The default constructor for the class is { CamelCaseText = false, AllowIntegerValues = true, CanRead = true, CanWrite = true }
        //private static StringEnumConverter _enumConverter = new StringEnumConverter();

        ///// <summary>
        ///// Serializes all objects from Pascal to camelCase. 
        ///// Check this: http://aspnetwebstack.codeplex.com/discussions/536733 & http://aspnetwebstack.codeplex.com/workitem/1739
        ///// +
        ///// With the StringEnumConverter serializes all enum values to string values
        ///// Documentation : https://github.com/JamesNK/Newtonsoft.Json/blob/master/Src/Newtonsoft.Json/Converters/StringEnumConverter.cs
        ///// </summary>
        ///// <returns></returns>
        //internal static JsonMediaTypeFormatter CamelCaseFormatter()
        //{
        //    _jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

        //    // Set the AllowIntegerValues to false in order to achieve the desired functionality: Enums => Json String values
        //    _enumConverter.AllowIntegerValues = false;
        //    // Add the converter to the jsonFormatter
        //    _jsonFormatter.SerializerSettings.Converters.Add(_enumConverter);

        //    return _jsonFormatter;
        //}
    }
}

﻿using Microsoft.IdentityModel.Protocols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using esend.Data;
using esend.Models;
using esend.Services;
using esend.Globalization;
using esend.Framework;
using Microsoft.AspNetCore.Mvc.Razor;
using System.Net.Http;
using System.Web;
using esend.Rest.CustomClients;
using esend.Models.Parameter;
using System.Runtime.Serialization.Json;
using System.Text;

namespace esend.Rest
{
    internal static class RestCallClient
    {
        #region  Global Configuration Properties

        private static readonly string GoogleApiPlacesAutocompleteEndpoint = "https://maps.googleapis.com/maps/api/place/autocomplete/json";
        private static readonly string GoogleApiPlacesKey = "AIzaSyBhlxnUzj2f5Bn4bdtkduZOXmsnTl7pxTQ";
        private static readonly string systemLanguage = "el";


        //private static readonly string Uri = System.Configuration.GetConnectionString("DefaultConnection");
        //private static readonly string RestEndpoint = ConfigurationManager.AppSettings["RestEndpoint"];
        //// private static readonly string AuthUrl = ConfigurationManager.AppSettings["openIdResourceUri"] + ConfigurationManager.AppSettings["oidAuthUri"];
        //private static readonly string AuthTokenUri = ConfigurationManager.AppSettings["openIdResourceUri"] + ConfigurationManager.AppSettings["oidTokenUri"];
        //private static readonly string AuthLogoutUri = ConfigurationManager.AppSettings["openIdResourceUri"] + ConfigurationManager.AppSettings["oidLogoutUri"];
        //private static readonly string AthenaFrontend = ConfigurationManager.AppSettings["athenaFrontend"];
        //private static readonly string AthenaFrontendPwd = ConfigurationManager.AppSettings["athenaFrontendPwd"];
        //private static readonly string RestRequestExceptionMessage = "Δεν μπορούμε να επικοινωνήσουμε με το server για αυτή την κλήση.";
        //private static readonly string RestRequestTimeOutExceptionMessage = "Η αναζήτηση με βάση τα κριτήρια σας δεν μπορεί να ολοκληρωθεί. Επιλέξτε μικρότερης κλίμακας δεδομένα και δοκιμάστε ξανά.";

        #endregion

        #region Method Uri Endpoints
        private static readonly string GoogleApiPlacesAutocompleteEndpointTest = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%CE%98%CE%B5%CF%83%CF%83%CE%B1%CE%BB%CE%BF%CE%BD%CE%AF%CE%BA%CE%B7&types=&language=el&types=cities&key=AIzaSyBhlxnUzj2f5Bn4bdtkduZOXmsnTl7pxTQ";

        private static readonly string GoogleApiPlacesCitiesQueryName = "cities";
        #endregion

        #region Constructor

        private static HttpClient client = EsendHttpClients.GetClient(); //new HttpClient(new CustomDelegatingHandler(),disposeHandler:false);

        private static readonly Type LoggerType = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;

        #endregion

        #region Google Api Places
        internal static async Task<GoogleApiPlacesResponse> GetPlaces(string queryString)
        {
            try
            {
                //var builder = new UriBuilder(GoogleApiPlacesAutocompleteEndpoint);
                //var query = HttpUtility.ParseQueryString(builder.Query);
                //query["input"] = queryString.ToString();
                //query["types"] = GoogleApiPlacesCitiesQueryName;
                //query["language"] = systemLanguage;
                //query["key"] = GoogleApiPlacesKey;
                //builder.Query = query.ToString();
                //var a = builder.ToString();
                //var stringTask = client.GetStreamAsync(builder.ToString());

                var strBuilder = new StringBuilder();
                strBuilder.Append(GoogleApiPlacesAutocompleteEndpoint);
                strBuilder.Append("?input=" + Uri.EscapeDataString(queryString));
                strBuilder.Append("&types=" + "");
                strBuilder.Append("&language=" + systemLanguage);
                strBuilder.Append("&key=" + GoogleApiPlacesKey);

                var stringTask = client.GetStreamAsync(strBuilder.ToString());

                var result = await stringTask;

                var serializer = new DataContractJsonSerializer(typeof(GoogleApiPlacesResponse));
                GoogleApiPlacesResponse places = serializer.ReadObject(await stringTask) as GoogleApiPlacesResponse;
                return places;
                //HttpResponseMessage response = client.GetAsync(builder.ToString()).Result;
                //return response;
            }
            catch (AggregateException ex)
            {
                throw ex;

                //LoggerWrapper.LogMsg(LoggerType, LogginLevelEnum.ERROR, ex.Message, ex); throw new RestHttpClientRequestException(RestRequestExceptionMessage, ex.StackTrace);
            }
            catch (UnauthorizedAccessException) { throw; }
            catch (Exception ex)
            {
                throw ex;
                //if (ex.GetType() == typeof(RestHttpClientUnAuthorizedException)) { throw; } else { LoggerWrapper.LogMsg(LoggerType, LogginLevelEnum.ERROR, ex.Message, ex); throw new RestHttpClientRequestException(ex.Message, ex.StackTrace); }
            }
        }
        #endregion
    }
}

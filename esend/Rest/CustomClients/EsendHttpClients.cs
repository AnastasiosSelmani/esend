﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace esend.Rest.CustomClients
{
    public static class EsendHttpClients
    {
        //public EsendHttpClients() : base()
        //{
        //    DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //    DefaultRequestHeaders.AcceptLanguage.Add(new StringWithQualityHeaderValue("el"));
        //}

        //public EsendHttpClients(string uri, string mediaTypeHeaderValue) : base()
        //{
        //    BaseAddress = new Uri(uri);
        //    DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(mediaTypeHeaderValue));
        //    DefaultRequestHeaders.AcceptLanguage.Add(new StringWithQualityHeaderValue("el"));
        //}

        public static HttpClient GetClient()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("User-Agent", ".NET Foundation Repository Reporter");

            return client;
        }
    }
}

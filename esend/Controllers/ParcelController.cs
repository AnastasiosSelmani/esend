﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace esend.Controllers
{
    [Route("[controller]/[action]")]
    public class ParcelController : Controller
    {
        private readonly ILogger _logger;

        private const string AuthenicatorUriFormat = "otpauth://totp/{0}:{1}?secret={2}&issuer={0}&digits=6";

        public ParcelController(
            ILogger<ManageController> logger)
        {
            _logger = logger;
        }

        public IActionResult GetParcelHistory(string trackNumber)
        {
            return View();
        }
    }
}
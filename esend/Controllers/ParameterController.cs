﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using esend.Models.Parameter;
using esend.Management.GoogleApisManagement;

namespace esend.Controllers
{
    public class ParameterController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> GetCities(string query)
        {
            //List<CityInputViewModel> cities = new List<CityInputViewModel>();
            List<GoogleApiPlace> places = await GoogleApisPlacesManagement.GetPlaces(query);
            //cities.Add(new CityInputViewModel() { Id = "1", Description = "Θεσσαλονίκη" });
            //if (ModelState.IsValid)
            //{
            //    try
            //    {
            //        AcademicClassSearchRequestDto dto = AthenaMapperConfiguration.Mapper.Map<AcademicClassSearchFlattened, AcademicClassSearchRequestDto>(model);
            //        AcademicClassSearchResultDto dtos = _academicManagement.searchAcademicClasses(dto, 0, 0);
            //        classes = AthenaMapperConfiguration.Mapper.Map<List<AcademicClassDto>, List<AcademicClass>>(dtos.Classes);
            //    }
            //    catch (RestHttpClientException ex)
            //    {
            //        LogException(ex);
            //    }
            //}
            places.OrderBy(o => o.description);
            return Json(places);
        }
    }
}
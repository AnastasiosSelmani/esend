﻿using esend.Globalization;

namespace esend.Framework
{
    public class CustomDisplayNameAttribute : System.ComponentModel.DisplayNameAttribute
    {

        private static IResourceService _resourceService;
        private string _resourceName;
        private string _resourceKey;

        public CustomDisplayNameAttribute(string resourceName, string resourceKey)
        {
            _resourceName = resourceName;
            _resourceKey = resourceKey;
        }

        public override string DisplayName
        {
            get
            {
                return _resourceService.GetResource(_resourceName, _resourceKey);
            }
        }

        public static void RegisterProvider(IResourceService provider)
        {
            _resourceService = provider;
        }
    }
}

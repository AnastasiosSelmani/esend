﻿using esend.Globalization;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace esend.Framework
{
    public static class ResourceHelper
    {
        private static IResourceService _resources;

        public static string GetResource(this HtmlHelper helper, string resourceName, string resourceKey)
        {
            return _resources.GetResource(resourceName, resourceKey);
        }

        public static void RegisterProvider(IResourceService provider)
        {
            _resources = provider;
        }
    }
}

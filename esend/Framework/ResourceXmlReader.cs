﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace esend.Framework
{
    //http://anthonygiretti.com/2014/09/how-to-manage-easily-different-languages-on-a-website-complete-tutorial-with-asp-net-mvc/
    public static class ResourceXmlReader
    {
        //static property, public readable only
        public static readonly Dictionary<string, Dictionary<string, Dictionary<string, string>>> Resources = new Dictionary<string, Dictionary<string, Dictionary<string, string>>>();
        private static readonly Type loggerType = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;

        //static constructor
        static ResourceXmlReader()
        {
            try
            {
                string path = "~/App_GlobalResources/XmlResources/";// System.Web.Hosting.HostingEnvironment.MapPath("~/App_GlobalResources/XmlResources/");
                DirectoryInfo d = new DirectoryInfo(path);//Assuming Test is your Folder
                FileInfo[] Files = d.GetFiles("*.xml"); //Getting Text files

                foreach (FileInfo file in Files)
                {
                    OpenAndStoreResource(file.Name);
                }
            }
            catch (Exception ex)
            {
                //Logging functionality
                //LoggerWrapper.LogUserMsg(loggerType, LogginLevelEnum.ERROR, ex.Message, ex);
            }
        }

        //Open, read and store into the static property xml file
        private static void OpenAndStoreResource(string resourcePath)
        {
            try
            {

                string fileName = Path.GetFileName(resourcePath).Split('.')[0];
                //XDocument doc = XDocument.Load(System.Web.Hosting.HostingEnvironment.MapPath("~/App_GlobalResources/XmlResources/") + resourcePath);
                XDocument doc = XDocument.Load("~/App_GlobalResources/XmlResources/" + resourcePath);

                if (doc.Descendants().Any())
                {
                    Dictionary<string, Dictionary<string, string>> currentResource = new Dictionary<string, Dictionary<string, string>>();

                    var resources = doc.Descendants("Resource").ToList();
                    resources.ForEach(o => currentResource.Add(o.Attribute("key").Value, getEachLanguage(o.Elements("Language"))));

                    //attach the resources to one uniwue resource
                    Resources.Add(fileName, currentResource);
                }
            }
            catch (Exception ex)
            {
                //Logging functionality
                //LoggerWrapper.LogUserMsg(loggerType, LogginLevelEnum.ERROR, resourcePath + ":" + ex.Message, ex);
            }
        }

        //Loop on each language into the file
        private static Dictionary<string, string> getEachLanguage(IEnumerable<XElement> elements)
        {
            Dictionary<string, string> langList = new Dictionary<string, string>();

            elements.ToList().ForEach(o => langList.Add(o.Attribute("key").Value, o.Value));

            return langList;
        }
    }
}

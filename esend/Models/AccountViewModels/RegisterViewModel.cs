﻿using esend.Framework;
using esend.Globalization;
using esend.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using esend.Models.Enumerators;

namespace esend.Models.AccountViewModels
{
    public class RegisterViewModel
    {
        //public ResourceService _resourceService = new ResourceService();

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        //[CustomDisplayName(XmlResourceNames.RegisterLoginXml, "LastName")]
        [Display(Name = "LastName")]
        public string LastName { get; set; }

        [Display(Name = "Title")]
        public string Title { get; set; }

        [Display(Name = "BirthDate")]
        public DateTime? BirthDate { get; set; }

        [Display(Name = "CellPhoneNumber")]
        public string CellPhoneNumber { get; set; }

        [Display(Name = "PhoneNumber")]
        public string PhoneNumber { get; set; }

        [Required]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Display(Name = "Town")]
        public string Town { get; set; }// Probably object

        [Display(Name = "ZipCode")]
        public Int32 ZipCode { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Floor")]
        public string Floor { get; set; }

        [Required]
        //[StringLength(100, ErrorMessage = _resourceService.GetResource(XmlResourceNames.RegisterLoginXml, "PasswordLengthValidation"), MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        //[Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        [Display(Name = "ConfirmPassword")]
        public string ConfirmPassword { get; set; }

        public UserType UserType { get; set; }
    }

    public class RegisterViewModelPrivate : RegisterViewModel
    {

    }

    public class RegisterViewModelBussiness : RegisterViewModel
    {
        [Required]
        [Display(Name = "BussinessTitle")]
        public string BussinessTitle { get; set; }

        [Display(Name = "TaxNumber")]
        public Int32 TaxNumber { get; set; } // ΑΦΜ

        [Display(Name = "TaxOffice")]
        public string TaxOffice { get; set; } // ΔΟΥ
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace esend.Models.Common
{
    public static class XmlResourceNames
    {
        public const string RegisterLoginXml = "RegisterLogin";
        public const string IndexXml = "Index";
    }
}

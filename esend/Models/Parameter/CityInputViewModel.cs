﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace esend.Models.Parameter
{
    public class CityInputViewModel
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string Place_Id { get; set; }
        public StructuredFormat Structured_Formatting { get; set; }
    }
}

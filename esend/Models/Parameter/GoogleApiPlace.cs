﻿
namespace esend.Models.Parameter
{
    public class GoogleApiPlace
    {
        public string id { get; set; }
        public string description { get; set; }
        public string place_id { get; set; }
        public StructuredFormat structured_formatting { get; set; }
    }

    public class StructuredFormat
    {
        public string main_text { get; set; }
    }
}

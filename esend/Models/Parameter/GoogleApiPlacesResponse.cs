﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace esend.Models.Parameter
{
    public class GoogleApiPlacesResponse
    {
        public string error_message { get; set; }
        public List<GoogleApiPlace> predictions { get; set; }
        public string status { get; set; }
    }
}

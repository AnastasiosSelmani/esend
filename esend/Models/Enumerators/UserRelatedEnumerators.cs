﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace esend.Models.Enumerators
{
    public enum UserType
    {
        PRIVATE = 1,
        BUSINESS = 2
    }
}
